@extends('layouts.app')
@section('content')
@section('title', 'users')
        <h1>Users</h1>
        <table class = "table table-dark">
            <tr> 
                <th>Id</th><th>Name</th><th>Email</th>><th>Department</th><th>Created</th><th>Updated</th>
            </tr>
        
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>
            @if(Gate::allows('delete-user'))
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Assign department
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($departments as $department)
                      <a class="dropdown-item" href="{{route('user.changeDepartment',[$department->id,$user->id])}}">{{$department->name}}</a>
                     @endforeach
                    </div>                
            </td>
            @else
            <td>{{$user->department->name}}
            @endif 
            </td>
            <td>{{$user->role_id}}</td>
            <td>{{$user->created_at}}</td>
            <td>{{$user->updated_at}}</td>
            <td>
            @if(Gate::allows('delete-user'))
            <a href = "{{route('user.delete',$user->id)}}">Delete</a>
            @endif 
            </td>
            <td>
            <a href = "{{route('user.myuser',$user->id)}}">details</a>
            </td>
        </tr>
        @if(Gate::allows('delete-user'))
        <form method="POST" action="{{ route('user.changeDepartment') }}">
        @csrf  
        <div class="form-group row">
        <label for="user" class="col-md-4 col-form-label text-md-right">Department</label>
        <div class="col-md-6">
            <select class="form-control" name="department_id">                                                                         
              @foreach ($departments as $department)
              @if($user->department_id == $department->id)
              <option value="{{ $department->id }}" selected = "selected"> 
                  {{ $department->name }} 
              </option>
              @else
              <option value="{{ $department->id }}"> 
                {{ $department->name }} 
                </option>
                @endif
              @endforeach    
            </select>
        </div>
        <input name="id" type="hidden" value = {{$user->id}} >
        <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        Update department
                    </button>
                </div>
        </div>                    
    </form>
    @endif 
        </table>
@endsection
