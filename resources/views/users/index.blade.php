@extends('layouts.app')
@section('content')
@section('title', 'users')
        <h1>Users</h1>
        <table class = "table table-dark">
            <tr> 
                <th>Id</th><th>Name</th><th>Email</th>><th>Department</th><th>Role</th><th>Created</th><th>Updated</th>
            </tr>
        @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->department->name}}</td>
            <td>{{$user->role_id}}</td>
            <td>{{$user->created_at}}</td>
            <td>{{$user->updated_at}}</td>
            <td>
            @if(Gate::allows('delete-user'))
            <a href = "{{route('user.delete',$user->id)}}">Delete</a>
            @else
            <a href = "">Delete</a>
            @endif 
            </td>
            <td>
            <a href = "{{route('user.myuser',$user->id)}}">details</a>
            </td>
        </tr>
        @endforeach
        </table>
@endsection
