<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
        'user_id'
    ];
    public function users()
    {
        return $this->hasMany('App\User');
    } 

}
