<?php

use Illuminate\Database\Seeder;
use carbon\carbon;   //carbon library

class CandidatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //insert *Manual* records into a table in the DB(random/real) 
        DB::table('candidates')->insert([
            'name' => Str::random(10),
            'email' => Str::random(5).'@Gmail.com',
            //Challenge
            'created_at' => carbon::now(),
            'updated_at' => carbon::now(),
        ]);
        //TODO
        /*insert *auto* records into a table in the DB(when i need to test big numbers of records) 
        factory(App\User::class, 50)->create()->each(function ($user) {
            $user->posts()->save(factory(App\Post::class)->make());
        });*/
    }
}
