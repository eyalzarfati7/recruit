<?php

use Illuminate\Database\Seeder;
use carbon\carbon;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            [
                'name' => 'HR',
                'created_at' => carbon::now(),
                'updated_at' => carbon::now(),
            ],
            [ 
                'name' => 'Managment',
                'created_at' => carbon::now(),
                'updated_at' => carbon::now(),
            ],
            ]);
        }
    
    }
    
    

